package com.taopanfeng.protocol;

import java.io.IOException;
import java.io.OutputStream;

/**
 * 协议层
 *
 * @author 陶攀峰
 * @version 1.0
 * @date 2021/3/29 下午6:05
 */
public class MyProtocol {

    public static final String X = "*";// 数组
    public static final String S = "$";// 字符串
    public static final String CRLF = "\r\n";// 换行

    /**
     * 组装发送的数据
     *
     * @author 陶攀峰
     * @date 2021/3/29 下午6:07
     */
    public static void sendCommand(Command command, OutputStream out, byte[]... bytes) {
        StringBuffer sb = new StringBuffer();

        sb.append(X).append(bytes.length + 1).append(CRLF);// *3

        sb.append(S).append(command.name().length()).append(CRLF);// $3
        sb.append(command).append(CRLF);// SET

        for (byte[] b : bytes) {
            sb.append(S).append(b.length).append(CRLF);// $2 $23
            sb.append(new String(b)).append(CRLF);// k1 2021-03-29T17:55:12.291
        }

        try {
            out.write(sb.toString().getBytes());
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    /**
     * 使用枚举，规定哪些命令可用
     *
     * @author 陶攀峰
     * @date 2021/3/30 上午9:24
     */
    public enum Command {
        SET, GET
    }

}
