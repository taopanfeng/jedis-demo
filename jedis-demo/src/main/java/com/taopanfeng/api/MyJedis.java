package com.taopanfeng.api;

import com.taopanfeng.connection.MyConnection;
import com.taopanfeng.protocol.MyProtocol;

/**
 * api 操作层
 *
 * @author 陶攀峰
 * @version 1.0
 * @date 2021/3/29 下午5:36
 */
public class MyJedis {
    private MyConnection myConnection;

    public MyJedis(String host, int port) {
        myConnection = new MyConnection(host, port);
    }

    public String set(final String key, final String value) {
        myConnection.sendCommand(MyProtocol.Command.SET, key.getBytes(), value.getBytes());
        return null;
    }

    public String get(final String key) {
        myConnection.sendCommand(MyProtocol.Command.GET, key.getBytes());
        return myConnection.getReply();
    }
}
