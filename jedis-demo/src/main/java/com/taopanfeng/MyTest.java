package com.taopanfeng;

import com.taopanfeng.api.MyJedis;

/**
 * 测试
 *
 * @author 陶攀峰
 * @version 1.0
 * @date 2021/3/29 下午5:29
 */
public class MyTest {

    //2021/3/29 下午5:30 测试 jedis
    /*public static void main(String[] args) throws Exception {
        Jedis jedis = new Jedis("127.0.0.1", 6379);

        Set<String> keys = jedis.keys("*");
        System.out.println(keys);// [k1, name]

        jedis.set("k1", LocalDateTime.now().toString());

        String k1 = jedis.get("k1");
        System.out.println(k1);// 2021-03-30T09:00:58.932
    }*/

    //2021/3/29 下午5:30 发送数据给 Hack，先启动 Hack
    /*public static void main(String[] args) throws Exception {
        Jedis jedis = new Jedis("127.0.0.1", 18019);

        jedis.set("k1", LocalDateTime.now().toString());

        String k1 = jedis.get("k1");
        System.out.println(k1);
    }*/

    //2021/3/29 下午6:20 测试 MyJedis set
    /*public static void main(String[] args) throws Exception {
        MyJedis myJedis = new MyJedis("127.0.0.1", 6379);
        myJedis.set("name", "陶攀峰");
    }*/

    //2021/3/30 上午8:30 测试 MyJedis get
    public static void main(String[] args) throws Exception {
        MyJedis myJedis = new MyJedis("127.0.0.1", 6379);
        String k1 = myJedis.get("name");
        System.out.println(k1);
        //$9
        //陶攀峰
    }
}
