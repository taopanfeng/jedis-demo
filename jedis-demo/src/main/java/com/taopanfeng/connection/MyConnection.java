package com.taopanfeng.connection;

import com.taopanfeng.protocol.MyProtocol;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.Socket;

/**
 * 连接层（连接 Redis 数据库）
 *
 * @author 陶攀峰
 * @version 1.0
 * @date 2021/3/29 下午5:37
 */
public class MyConnection {
    private String host;
    private int port;

    private Socket socket;

    private InputStream in;
    private OutputStream out;

    public MyConnection(String host, int port) {
        this.host = host;
        this.port = port;
    }

    private void connect() {
        try {
            socket = new Socket(host, port);
            in = socket.getInputStream();
            out = socket.getOutputStream();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }


    public MyConnection sendCommand(MyProtocol.Command command, byte[]... bytes) {
        // 连接
        connect();
        // 发送
        MyProtocol.sendCommand(command, out, bytes);

        return this;
    }

    /**
     * 从 socket 读取数据
     *
     * @author 陶攀峰
     * @date 2021/3/30 上午8:29
     */
    public String getReply() {
        byte[] bytes = new byte[1024];

        try {
            socket.getInputStream().read(bytes);
        } catch (IOException e) {
            e.printStackTrace();
        }

        return new String(bytes);
    }
}
