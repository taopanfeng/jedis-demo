package com.taopanfeng;

import java.io.InputStream;
import java.net.ServerSocket;
import java.net.Socket;

/**
 * 黑客：拦截数据
 *
 * @author 陶攀峰
 * @version 1.0
 * @date 2021/3/29 下午5:47
 */
public class Hack {

    //2021/3/29 下午5:47 黑客：拦截数据（先启动自己，监听着，再启动 MyTest 18019）
    public static void main(String[] args) throws Exception {
        ServerSocket serverSocket = new ServerSocket(18019);// 使用一个本地未被占用的端口
        Socket socket = serverSocket.accept();

        InputStream in = socket.getInputStream();

        byte[] bytes = new byte[1024];
        in.read(bytes);

        System.out.println(new String(bytes));
        // *3
        // $3
        // SET
        // $2
        // k1
        // $23
        // 2021-03-29T17:55:12.291


        // 1、打开 https://redis.io/documentation
        // 2、搜索 protocol

        // 3、找到 Specifications ===> Redis Protocol specification ===> <https://redis.io/topics/protocol>
        //Specifications
        //Redis Design Drafts: Design drafts of new proposals.
        //Redis Protocol specification: if you're implementing a client, or out of curiosity, learn how to communicate with Redis at a low level.
        //Redis RDB format specification, and RDB version history.
        //Internals: Learn details about how Redis is implemented under the hood.

        // 4、找到 RESP protocol description ===> 翻译中文，如下
        //RESP协议描述
        //RESP协议在Redis 1.2中引入，但它成为Redis 2.0中与Redis服务器对话的标准方式。这是您应该在Redis客户端中实现的协议。
        //RESP实际上是一种序列化协议，支持以下数据类型：简单字符串、错误、整数、散装字符串和数组。
        //RESP在Redis中用作请求响应协议的方式如下：
        //客户端将命令作为散装字符串的RESP数组发送到Redis服务器。
        //服务器根据命令实现使用RESP类型之一进行响应。
        //在RESP中，某些数据的类型取决于第一个字节：
        //对于简单字符串，回复的第一个字节是“+”
        //对于错误，回复的第一个字节是“-”
        //对于整数来说，回复的第一个字节是“:”
        //对于散装字符串，回复的第一个字节是“$”
        //对于数组，回复的第一个字节是“*”
        //此外，RESP可以使用稍后指定的批量字符串或数组的特殊变体表示空值。
        //在RESP中，协议的不同部分总是以“\r\n”（CRLF）结束。

        // 5、翻译上方内容
        // *3 ===> 数组有 3个【（$3,SET），（$2,k1），（$23,2021-03-29T17:55:12.291）】
        // $3 ===> 字符串 3个【（S），（E），（T）】
        // SET
        // $2 ===> 字符串 2个【（k），（1）】
        // k1
        // $23 ===> 字符串 23个
        // 2021-03-29T17:55:12.291
    }
}
